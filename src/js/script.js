/* Copyright 2018 Nicholas Gilbert
    Licensed under the MIT license, refer to LICENSE.md 
    in the root of the repository for details
*/

var linuxEquinox = new Date("1991-09-17");

function calcLE() {
    console.log("Hello there!");
    let dateIn = document.getElementsByName("birthday");
    console.log(dateIn[0].value);
    let dateVal = new Date(dateIn[0].value);
    let diff = dateVal - linuxEquinox;
    diff = diff / 1000; // milliseconds to seconds
    diff = diff / 60; // seconds to minutes
    diff = diff / 60; // minutes to hours
    diff = diff / 24; // hours to day
    diff = diff / 365; // day to year
    let postfix = "";
    diff > 0 ? postfix = "PL" : postfix = "BL";
    diff = Math.abs(diff);

    alert(diff.toFixed(3) + " " + postfix);
}